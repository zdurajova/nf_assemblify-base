#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##              Quality control of reads using fastqc                        ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/fastqc.nf process
args=("$@")
FASTQ_FILE=${args[0]}
LOGCMD=${args[1]}

# Command to execute
CMD="fastqc ${FASTQ_FILE} -o ."

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

