#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Quality control of reads using nanoplot                  ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
INPUT=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="NanoPlot -t ${NCPUS} --plots kde dot hex --N50 \
         --title \"PacBio Hifi reads for $(basename ${INPUT%.hifi*})\" \
         --fastq ${INPUT} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

