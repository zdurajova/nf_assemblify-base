#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           Genome assembly repeats softmasking using Redmask               ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
INPUT=${args[1]}
LOGCMD=${args[2]}



CMD="Red -cor $NCPUS -gnm . -msk masked"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Output for Red (a new directory)
mkdir masked

# Run Command
eval ${CMD}

