#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                        Reads indexing using Samtools                      ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
INPUT=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}

CMD1="samtools sort -o $(basename ${INPUT%.*}).bam -@ $NCPUS -m 2G -O bam -T tmp $INPUT"
CMD2="samtools index $(basename ${INPUT%.*}).bam"

# Keep command in log
echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

# Run Command
eval ${CMD1}
eval ${CMD2}