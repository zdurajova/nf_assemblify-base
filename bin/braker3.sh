#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                        Genome annotation with BRAKER3                     ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
RED=${args[1]}
BAM=${args[2]}
PROT=${args[3]}
SPECIES=${args[4]}
LOGCMD=${args[5]}

braker.pl --species=${SPECIES} --genome=${RED} --bam=${BAM} --prot_seq=${PROT} \
          --workingdir=. --gff3 --threads ${NCPUS}
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD