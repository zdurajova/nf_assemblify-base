#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                         Reads mapping using Hisat2                        ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
MSK=${args[1]}
R1=${args[2]}
R2=${args[3]}
LOGCMD=${args[4]}

CMD="hisat2-build -p $NCPUS $MSK ${MSK%.*} ; hisat2 -p $NCPUS --no-unal -q -x ${MSK%.*} -1 ${R1} -2 ${R2} --max-intronlen 20000 > hisat2_output.sam"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD