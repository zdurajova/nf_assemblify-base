#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Run reads assembly using hifiasm                     ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
HIFI=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="hifiasm -k 51 -t ${NCPUS} ${HIFI}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

# Check hifiasm return code
if [ ! $? -eq 0 ]; then
  echo "ERROR: hifiasm failed"
  exit 1
fi

# if ok, post-process hifi result files
for gfa in *.p_ctg.gfa
do
  awk '/^S/{print ">"$2;print $3}' ${gfa} > ${gfa%.*}.fa
done


