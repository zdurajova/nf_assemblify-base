#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           Quantitative assessment of genome assembly with BUSCO           ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
BUSCO_DB_PATH=${args[1]}
BUSCO_DB_NAME=${args[2]}
INPUT=${args[3]}
LOGCMD=${args[4]}

# Command to execute
CMD="busco -c $NCPUS --offline -i $INPUT --mode proteins -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o busco_on_braker3_output"

# Keep command in log
echo $CMD > $LOGCMD

# Execute command
eval ${CMD}
