process braker3 {

    label 'highmem'

    tag "braker3"

    publishDir "${params.resultdir}/05a_braker3",	mode: 'copy', pattern: '*.gff3'
    publishDir "${params.resultdir}/05a_braker3",	mode: 'copy', pattern: '*.aa'
    publishDir "${params.resultdir}/logs/braker3",	mode: 'copy', pattern: 'braker3.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'braker3.cmd'

    input:
        path(redmask_ch)
        path(samtools_bam_ch)
        path(braker3_prot_ch)
        val(braker3_species_ch)

    output:
        path("*.gff3"), emit: gff3_ch
        path("*.aa"), emit: faa_ch
        path("*.log")
        path("*.cmd")
        
    script:
    """
    braker3.sh ${task.cpus} $redmask_ch $samtools_bam_ch $braker3_prot_ch $braker3_species_ch braker3.cmd >& braker3.log 2>&1
    """ 
}
