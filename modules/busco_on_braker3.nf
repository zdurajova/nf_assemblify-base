process busco_on_braker3 {
    label 'midmem'

    tag "busco_on_braker3"

    publishDir "${params.resultdir}/05b_busco",	mode: 'copy', pattern : 'busco_on_braker3_output'
    publishDir "${params.resultdir}/logs/busco",	mode: 'copy', pattern : 'busco_on_braker3.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'busco_on_braker3.cmd'

    input:
        path(busco_db_path)
        val(busco_db_name)
        path(faa_ch)

    output:
        path("busco_on_braker3_output")
        path("busco_on_braker3.log")
        path("busco_on_braker3.cmd")

    script:
    """
    busco_on_braker3.sh ${task.cpus} ${busco_db_path} ${busco_db_name} ${faa_ch} busco_on_braker3.cmd >& busco_on_braker3.log 2>&1
    """
}



