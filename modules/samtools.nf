process samtools {
    label 'midmem'

    tag "samtools"

    publishDir "${params.resultdir}/04c_samtools",	mode: 'copy', pattern : '*.bam'
    publishDir "${params.resultdir}/04c_samtools",	mode: 'copy', pattern : '*.bai'
    publishDir "${params.resultdir}/logs/busco",	mode: 'copy', pattern : 'busco.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'busco.cmd'

    input:
        each(hisat2_sam_ch)

    output:
        path("*.bam"), emit: samtools_bam_ch
        path("*.bai")
        path("samtools.log")
        path("samtools.cmd")

    script:
    """
    samtools.sh ${hisat2_sam_ch} ${task.cpus} samtools.cmd >& samtools.log 2>&1
    """
}



