process nanoplot {

    label 'lowmem'

    tag "nanoplot"

    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.png'
    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/nanoplot",	mode: 'copy', pattern: 'nanoplot.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'nanoplot.cmd'

    input:
      	each(pacbio_ch)

    output:       
        path("*.txt")
        path("*.png")
        path("*.html")
        path("nanoplot.log")
        path("nanoplot.cmd")

    script:
    """
    nanoplot.sh ${task.cpus} $pacbio_ch nanoplot.cmd >& nanoplot.log 2>&1
    """ 
}



