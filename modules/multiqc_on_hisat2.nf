process multiqc_on_hisat2 {

    label 'lowmem'

    tag "multiQC_on_hisat2"

    publishDir "${params.resultdir}/04b_multiqc_on_hisat2",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc",	mode: 'copy', pattern: 'multiqc_on_hisat2.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'multiqc_on_hisat2.cmd'

    input:
        path(hisat2_log_ch)

    output:
        path("*.html")
        path("multiqc_on_hisat2.log")
        path("multiqc_on_hisat2.cmd")

    script:
    """
    multiqc.sh "." "multiqc_report" multiqc_on_hisat2.cmd >& multiqc_on_hisat2.log 2>&1
    """ 
}



