process hisat2 {

    label 'highmem'

    tag "hisat2"

    publishDir "${params.resultdir}/04a_hisat2",	mode: 'copy', pattern: '*.ht2'
    publishDir "${params.resultdir}/04a_hisat2",	mode: 'copy', pattern: 'hisat2_output.sam'
    publishDir "${params.resultdir}/logs/hisat2",	mode: 'copy', pattern: 'hisat2.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hisat2.cmd'

    input:
        path(redmask_ch)
        path(r1_illumina_ch)
        path(r2_illumina_ch)

    output:
        path("*.ht2")
        path("hisat2_output.sam"), emit: hisat2_sam_ch
        path("hisat2.log"), emit: hisat2_log_ch
        path("hisat2.cmd")
        
    script:
    """
    hisat2.sh ${task.cpus} $redmask_ch $r1_illumina_ch $r2_illumina_ch hisat2.cmd >& hisat2.log 2>&1
    """ 
}
